<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder{

	public function run()
	{
		\DB::table('users')->insert(array(
			'name' => 'adrian',
			'email' => 'azurmedia.ad@gmail.com',
			'password' => \Hash::make('secret')
			)
		);

	}

}